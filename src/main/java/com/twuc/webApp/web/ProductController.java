package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(value = "/api")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping(value = "/products")
    public ResponseEntity<GetProductResponse> createProduct(@RequestBody @Valid CreateProductRequest request) {
        Product product = request.toProduct();

        productRepository.save(product);
        productRepository.flush();

        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location", String.format("%s/api/products/%d","http://localhost", product.getId()))
                .body(new GetProductResponse(product));
    }

    @GetMapping(value = "/products/{productId}")
    public ResponseEntity<GetProductResponse> getProduct(@PathVariable(value = "productId") Long id) {
        Product product = productRepository.findById(id).orElseThrow(NoSuchElementException::new);

        return ResponseEntity.status(HttpStatus.OK)
                .body(new GetProductResponse(product));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> validationHandler(MethodArgumentNotValidException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body("Something wrong with the argument");
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<String> noSuchElementExceptionHandler(NoSuchElementException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("No such product");
    }

}